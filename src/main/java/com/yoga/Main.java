package com.yoga;

public class Main {

    public static void main(String[] args) {
        //Soal 1
        for(int i=0;i<3;i++){
            for(int j=0;j<=i;j++) {
                System.out.print("*");
            }
            System.out.println("");
        }
        System.out.println("");

        //Soal 2
        for(int i=1; i<=3; i++){
            for(int j=1; j <= 3; j++){
                if(j < i){
                    System.out.print(" ");
                } else {
                    System.out.print("*");
                }
            }
            System.out.print("\n");
        }
    }
}
